
import java.awt.Color;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import static javax.swing.JOptionPane.showMessageDialog;
import java.sql.*;


public class RegisterForm extends javax.swing.JFrame {

    String name="";
    String erp="";
    String email="";
    String contNo="";
    String pass="";
    int j,i=0;
    
    String type="Student";
  String access = "true";
    public RegisterForm() {
        initComponents();
        jRadioButton2.setSelected(true);
    }
    public void getData(){
        name = jTextField1.getText();
        erp = jTextField2.getText();
        email = jTextField3.getText();
        contNo = jTextField4.getText();
        pass =jTextField5.getText();
    
    }
    public void checkData(){
        i=0; j=0;
        if(name.equals("")){
             jLabel1.setForeground(Color.RED);
             i++;
        }
        else
            jLabel1.setForeground(Color.BLACK);
        if(erp.equals("")){
            jLabel2.setForeground(Color.RED);
            i++;
        }
        else
            jLabel2.setForeground(Color.BLACK);
        if(email.equals("")){
            jLabel3.setForeground(Color.RED);
            i++;
        }
        else
            jLabel3.setForeground(Color.BLACK);
        if(contNo.equals("")){
            jLabel4.setForeground(Color.RED);
            i++;
        }
        else
            jLabel4.setForeground(Color.BLACK);
        if(pass.equals("")){
            jLabel5.setForeground(Color.RED);
            i++;
        }
        else
            {
                jLabel6.setForeground(Color.BLACK);
            if(!pass.equals(jTextField6.getText())){
                jLabel5.setForeground(Color.RED);
                jLabel6.setForeground(Color.RED);
                showMessageDialog(null, "Password didn't match, try again!");
                j++;
                }
            }
        if(i!=0){
            showMessageDialog(null, "Incomplete Data, try again.");
        }
        if(i==0 & j==0){
           insert(name, erp, contNo, email, pass, type, access);
        }
    }
    public void saveData(){
        
            String entry;
            RegisterForm a = new RegisterForm();
            a.getData();

            try{
                FileOutputStream fileOut = new FileOutputStream("src\\Entries\\kk.txt");
                ObjectOutputStream out = new ObjectOutputStream(fileOut);
                out.writeObject(a);
                out.close();
                fileOut.close();
                showMessageDialog(null, "Congratulations, your account has been created, login to continue.");

            } catch(IOException ex){
                System.out.println(ex.toString());
                System.out.println("Could not write.");

            }
    }
    public void insert(String name, String erp, String ContNo, String email, String Pass, String type, String access){
        try{
        Connection conn = DriverManager.getConnection(
                "jdbc:ucanaccess://E:\\Data\\File.accdb");
        Statement s = conn.createStatement();
//          
        String SQL = "INSERT INTO MyData VALUES (?, ?, ?, ?, ?, ?, ?)"; 
        PreparedStatement pstmt = conn.prepareStatement(SQL);
        
        pstmt.setString(1, name);
        pstmt.setString(2, erp);
        pstmt.setString(3, ContNo);
        pstmt.setString(4, email);
        pstmt.setString(5, Pass);
        pstmt.setString(6, type);
        pstmt.setString(7, access);
        
        pstmt.executeUpdate();
        pstmt.close();
        System.out.println("new record has been inserted in database");
        showMessageDialog(null, "New record has been inserted in Database");
        } catch(Exception e){
            System.out.println("can't insert");
            System.out.println(e.getMessage());
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jButton1 = new javax.swing.JButton();
        jTextField1 = new javax.swing.JTextField();
        jTextField2 = new javax.swing.JTextField();
        jTextField3 = new javax.swing.JTextField();
        jTextField4 = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jRadioButton1 = new javax.swing.JRadioButton();
        jRadioButton2 = new javax.swing.JRadioButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jButton2 = new javax.swing.JButton();
        jTextField5 = new javax.swing.JPasswordField();
        jTextField6 = new javax.swing.JPasswordField();
        jLabel10 = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenu2 = new javax.swing.JMenu();
        jMenu3 = new javax.swing.JMenu();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(0, 204, 204));
        jPanel1.setForeground(new java.awt.Color(51, 153, 255));
        jPanel1.setPreferredSize(new java.awt.Dimension(699, 485));
        jPanel1.setLayout(null);

        jButton1.setBackground(new java.awt.Color(0, 153, 153));
        jButton1.setFont(new java.awt.Font("Andalus", 0, 14)); // NOI18N
        jButton1.setText("Register");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton1);
        jButton1.setBounds(520, 400, 100, 31);

        jTextField1.setBackground(new java.awt.Color(0, 153, 153));
        jPanel1.add(jTextField1);
        jTextField1.setBounds(230, 100, 236, 20);

        jTextField2.setBackground(new java.awt.Color(0, 153, 153));
        jTextField2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField2ActionPerformed(evt);
            }
        });
        jPanel1.add(jTextField2);
        jTextField2.setBounds(230, 130, 236, 20);

        jTextField3.setBackground(new java.awt.Color(0, 153, 153));
        jTextField3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField3ActionPerformed(evt);
            }
        });
        jPanel1.add(jTextField3);
        jTextField3.setBounds(230, 160, 236, 20);

        jTextField4.setBackground(new java.awt.Color(0, 153, 153));
        jTextField4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField4ActionPerformed(evt);
            }
        });
        jPanel1.add(jTextField4);
        jTextField4.setBounds(230, 190, 236, 20);

        jLabel7.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(0, 153, 255));
        jLabel7.setText("- - Please enter following information to register - - ");
        jPanel1.add(jLabel7);
        jLabel7.setBounds(170, 60, 380, 24);

        jLabel8.setFont(new java.awt.Font("Andalus", 1, 36)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(0, 153, 255));
        jLabel8.setText("Registration");
        jPanel1.add(jLabel8);
        jLabel8.setBounds(230, 10, 230, 56);

        jRadioButton1.setBackground(new java.awt.Color(0, 153, 153));
        jRadioButton1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jRadioButton1.setForeground(new java.awt.Color(0, 255, 204));
        jRadioButton1.setText("Teacher");
        jRadioButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButton1ActionPerformed(evt);
            }
        });
        jPanel1.add(jRadioButton1);
        jRadioButton1.setBounds(380, 290, 80, 23);

        jRadioButton2.setBackground(new java.awt.Color(0, 153, 153));
        jRadioButton2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jRadioButton2.setForeground(new java.awt.Color(0, 255, 204));
        jRadioButton2.setText("Student");
        jRadioButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButton2ActionPerformed(evt);
            }
        });
        jPanel1.add(jRadioButton2);
        jRadioButton2.setBounds(230, 290, 80, 23);

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(0, 255, 204));
        jLabel1.setText("Name:");
        jPanel1.add(jLabel1);
        jLabel1.setBounds(10, 100, 112, 17);

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(0, 255, 204));
        jLabel2.setText("ERP:");
        jPanel1.add(jLabel2);
        jLabel2.setBounds(10, 130, 120, 22);

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(0, 255, 204));
        jLabel3.setText("Email ID:");
        jPanel1.add(jLabel3);
        jLabel3.setBounds(10, 160, 120, 22);

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(0, 255, 204));
        jLabel4.setText("Contact no:");
        jPanel1.add(jLabel4);
        jLabel4.setBounds(10, 190, 150, 22);

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(0, 255, 204));
        jLabel5.setText("New Password");
        jPanel1.add(jLabel5);
        jLabel5.setBounds(10, 220, 160, 20);

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(0, 255, 204));
        jLabel6.setText("Confirm Password");
        jPanel1.add(jLabel6);
        jLabel6.setBounds(10, 260, 180, 22);

        jLabel9.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel9.setForeground(new java.awt.Color(0, 255, 204));
        jLabel9.setText("Type:");
        jPanel1.add(jLabel9);
        jLabel9.setBounds(10, 290, 86, 23);

        jButton2.setBackground(new java.awt.Color(0, 153, 153));
        jButton2.setFont(new java.awt.Font("Andalus", 0, 14)); // NOI18N
        jButton2.setText("Login");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton2);
        jButton2.setBounds(20, 400, 80, 31);

        jTextField5.setBackground(new java.awt.Color(0, 153, 153));
        jPanel1.add(jTextField5);
        jTextField5.setBounds(230, 220, 236, 20);

        jTextField6.setBackground(new java.awt.Color(0, 153, 153));
        jTextField6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField6ActionPerformed(evt);
            }
        });
        jPanel1.add(jTextField6);
        jTextField6.setBounds(230, 250, 236, 20);

        jLabel10.setIcon(new javax.swing.ImageIcon("C:\\Users\\Anand\\Documents\\NetBeansProjects\\MyProject\\src\\background.jpg")); // NOI18N
        jPanel1.add(jLabel10);
        jLabel10.setBounds(0, 0, 700, 460);

        jMenu1.setText("Help");
        jMenuBar1.add(jMenu1);

        jMenu2.setText("About");
        jMenu2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jMenu2MouseClicked(evt);
            }
        });
        jMenuBar1.add(jMenu2);

        jMenu3.setText("Quit");
        jMenu3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jMenu3MouseClicked(evt);
            }
        });
        jMenuBar1.add(jMenu3);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 661, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 461, Short.MAX_VALUE))
        );

        setSize(new java.awt.Dimension(677, 521));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        getData();
        checkData();
       
       //checkData(); 
        
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        Form1 f=new Form1();
        f.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jMenu3MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jMenu3MouseClicked
        System.exit(0);
    }//GEN-LAST:event_jMenu3MouseClicked

    private void jMenu2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jMenu2MouseClicked
        showMessageDialog(null, "Plotit is an application to create an interaction"
                + "\nbetween student and teacher, providing them funtionalies:"
                + "\n- Graphing\n- Drawing Shapes\n- Statistical Visualisation\n- Quiz");
    }//GEN-LAST:event_jMenu2MouseClicked

    private void jRadioButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButton1ActionPerformed
        jRadioButton2.setSelected(false);
        type = "Teacher";
        access="false";
    }//GEN-LAST:event_jRadioButton1ActionPerformed

    private void jRadioButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButton2ActionPerformed
        jRadioButton1.setSelected(false);
        type = "Student";
        access="true";
    }//GEN-LAST:event_jRadioButton2ActionPerformed

    private void jTextField2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField2ActionPerformed

    private void jTextField3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField3ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField3ActionPerformed

    private void jTextField4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField4ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField4ActionPerformed

    private void jTextField6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField6ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField6ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(RegisterForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(RegisterForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(RegisterForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(RegisterForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new RegisterForm().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenu jMenu3;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JRadioButton jRadioButton1;
    private javax.swing.JRadioButton jRadioButton2;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JTextField jTextField2;
    private javax.swing.JTextField jTextField3;
    private javax.swing.JTextField jTextField4;
    private javax.swing.JPasswordField jTextField5;
    private javax.swing.JPasswordField jTextField6;
    // End of variables declaration//GEN-END:variables
}
