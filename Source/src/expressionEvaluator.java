
import java.util.Stack;

public class expressionEvaluator {


    
public boolean Validator(String infix){
if(Validity(infix)&&validityBra(infix)==true){
return true;
}
else return false;
}
    public static boolean isOperator(char a) {
        if (a == '+' || a == '-' || a == '*' || a == '/' || a == '^') {
            return true;
        } else {
            return false;
        }
    }

    public static boolean Validity(String s) {
        int i = 0;
        boolean x = false;
        while (i < s.length()) {
            if (isOperator(s.charAt(0)) || (isOperator(s.charAt(i)) && isOperator(s.charAt(i + 1))) || isOperator(s.charAt(s.length() - 1))) {
                x = false;
                break;
            }
            i++;
            x = true;
        }
        return x;
    }

    public static boolean validityBra(String s) {
        int i = 0;
        char symb;
        StackList<Character> list = new StackList<Character>();
        while (i < s.length()) {
            symb = s.charAt(i);
            if (symb == '(') {
                list.PUSH(symb);
            } else if (symb == ')') {
                if (list.isEmpty()) {
                    return false;
                } else {
                    char c = list.POP();
                    if (!(symb == '(' && c == ')' || symb == ')' && c == '(')) {
                        return false;
                    }
                }
            }
            i++;
        }
        if (list.isEmpty()) {
            return true;
        } else {
            return false;
        }
    }
}

class ShuntingYard {
     private DoubleStack memory;
    private CharStack operators;
    private String postfix;
    private double numbers[];
   
    public ShuntingYard() {
        memory = new DoubleStack();
        operators = new CharStack();
        numbers = new double[100];
      //  postfix = "";

    }

    public double getValue(String postfix, double x) {
        int i = 0;
        memory.clear();
        for (int n = 0; n < postfix.length(); n++) {
            char ch = postfix.charAt(n);
            if ('0' <= ch && ch <= '9') {
                memory.push(ch - '0');
            } else {
                switch (ch) {
                    case 'x':
                        memory.push(x);
                        break;
                    case '+':
                        double b = memory.pop();
                        double a = memory.pop();
                        memory.push(a + b);
                        break;
                    case '$':
                        memory.push(-memory.pop());
                        break;
                    case '-':
                        b = memory.pop();
                        a = memory.pop();
                        memory.push(a - b);
                        break;
                    case '*':
                        b = memory.pop();
                        a = memory.pop();
                        memory.push(a * b);
                        break;
                    case '/':
                        b = memory.pop();
                        a = memory.pop();
                        memory.push(a / b);
                        break;
                    case '^':
                        b = memory.pop();
                        a = memory.pop();
                        memory.push(Math.pow(a, b));
                        break;
                }
            }
        }
        return memory.pop();
    }
    public String replaceXPostfix(String postfix, double num){
       char numm  = (char) num;
        for (int i = 0; i < postfix.length(); i++) {
            if(postfix.charAt(i)>='a'||postfix.charAt(i)<='z' ){
              postfix=  postfix.replace(postfix.charAt(i), numm);
            }
        }
        
       return postfix;
    }

    public String infixToPostfix(String infix) {
        /* To find out the precedence, we take the index of the
           token in the ops string and divide by 2 (rounding down). 
           This will give us: 0, 0, 1, 1, 2 */
        final String ops = "-+/*^";

        StringBuilder sb = new StringBuilder();
        Stack<Integer> s = new Stack<>();

        for (String token : infix.split("\\s")) {
            if (token.isEmpty()) {
                continue;
            }
            char c = token.charAt(0);
            int idx = ops.indexOf(c);

            // check for operator
            if (idx != -1) {
                if (s.isEmpty()) {
                    s.push(idx);
                } else {
                    while (!s.isEmpty()) {
                        int prec2 = s.peek() / 2;
                        int prec1 = idx / 2;
                        if (prec2 > prec1 || (prec2 == prec1 && c != '^')) {
                            sb.append(ops.charAt(s.pop())).append(' ');
                        } else {
                            break;
                        }
                    }
                    s.push(idx);
                }
            } else if (c == '(') {
                s.push(-2); // -2 stands for '('
            } else if (c == ')') {
                // until '(' on stack, pop operators.
                while (s.peek() != -2) {
                    sb.append(ops.charAt(s.pop())).append(' ');
                }
                s.pop();
            } else {
                sb.append(token).append(' ');
            }
        }
        while (!s.isEmpty()) {
            sb.append(ops.charAt(s.pop())).append(' ');
        }
        return sb.toString();
    }
}

class StackList<T extends Comparable<T>> {

    T[] list;
    int size;
    int top = -1;

    StackList() {
        list = (T[]) new Comparable[10];
        size = 10;
    }

    public StackList(int s) {
        list = (T[]) new Object[s];
        size = s;
    }

    public void PUSH(T c) {
        if (isFull()) {
            System.out.println("Stack is Full!");
        } else {
            top++;
        }
        list[top] = c;
    }

    public T POP() {
        //isEmpty();
        T temp = list[top];
        top--;
        return temp;
    }

    public boolean isEmpty() {
        if (top == -1) {
            return true;
        } else {
            return false;
        }
    }

    public boolean isFull() {
        if (top == size - 1) {
            return true;
        } else {
            return false;
        }
    }

}
 class CharStack {

    private char data[];
    private int top;

    public CharStack() {
        data = new char[100];
        top = -1;
    }

    public void push(char c) {
        data[++top] = c;
    }

    public char pop() {
        if (top < 0) {
            return '(';
        }
        return data[top--];
    }

    public boolean empty() {
        if (top == -1) {
            return true;
        }
        return false;
    }

    public char peek() {
        if (top < 0) {
            return '(';
        }
        return data[top];

    }

    public void clear() {
        top = -1;
    }
}

class DoubleStack {

    private double data[];
    private int top;

    public DoubleStack() {
        data = new double[100];
        top = -1;
    }

    public void push(double c) {
        data[++top] = c;
    }

    public double pop() {
        return data[top--];

    }

    public boolean empty() {
        if (top == -1) {
            return true;
        }
        return false;
    }

    public void clear() {
        top = -1;
    }
}